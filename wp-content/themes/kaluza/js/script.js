/**
 * File script.js.
 *
 * Handles a variety of functionality such as section resizing based on browser width, transition handling etc
 */

jQuery(document).ready(function($) {

    /**
     * Initialise Rellax parallax scrolling script for blocks
     */
    if ($('.rellax').length > 0) {
        new Rellax('.rellax', {
            center: true
        });    
    }

    /**
     * Initialise background-image parallax script
     */
    new universalParallax().init({speed: 4.0});

    /**
     * Prevent scrolling by adding a class when a condition is true.
     * Class needs to overflow:hidden
     */
    function kaluzaToggleScrolling(condition) {
        var elem = $('html');
        var className = 'kaluza-no-scroll';
        if (condition) {
            elem.addClass(className).css('top',-scrollTop);
        } else {
            elem.removeClass(className);
        }
    }

    /**
     * Modal functionality
     */
    (function kaluzaModal() {

        var modalSelector = '.kaluza__modal';
        var modalRevealedClass = 'kaluza__modal--revealed';

        $(modalSelector).parent().on('click',function(e){
            if ($(e.target).hasClass('kaluza__modal') || $.contains(e.target)) return;
            var that = $(this).find(modalSelector);
            if (!that.hasClass(modalRevealedClass)) {
                that.addClass(modalRevealedClass);
                kaluzaToggleScrolling(that.hasClass(modalRevealedClass));
            }
        });
    
        $('.kaluza__modal-close').on('click',function(e){
            var modal = $(this).parent().parent();
            modal.removeClass(modalRevealedClass);
            kaluzaToggleScrolling(modal.hasClass(modalRevealedClass));
        });
    
        $(modalSelector).on('click', function(e){
            var target = $(this).hasClass(modalRevealedClass) ? $(this) : $(this).parents(modalSelector);
            if (target.hasClass(modalRevealedClass)) {
                target.removeClass(modalRevealedClass);
                kaluzaToggleScrolling(target.hasClass(modalRevealedClass));
            }
        });
    
        $('.kaluza__modal-inner').click(function(e){
            e.stopPropagation();
        })
    })();

    /**
     * Hamburger menu functionality
     */
    (function kaluzaHamburgerMenu() {

        var buttonSelector = 'button.hamburger';
        var buttonActiveClass = 'is-active';
        var menuSelector = '.menu-primary-container';
        var menuActiveClass = 'mobile-menu--revealed';
        var menuButtonSelector = $(menuSelector).find('a');

        // Close menu
        var closeMenu = function() { 
            $(buttonSelector).removeClass(buttonActiveClass);
            $(menuSelector).removeClass(menuActiveClass);
        }

        // Open menu
        var openMenu = function() {
            $(buttonSelector).addClass(buttonActiveClass);
            $(menuSelector).addClass(menuActiveClass); 
        }        

        // Open and close menu on clicking the hamburger
        $(buttonSelector).on('click', function(e) {
            if ($(this).hasClass(buttonActiveClass)) {
                closeMenu();
            } else {
                openMenu();
            }
            e.stopImmediatePropagation();
            e.preventDefault();
        });

        // Close menu on clicking on menu link
        menuButtonSelector.on('click', function() {
            if ($(menuSelector).hasClass(menuActiveClass)) {
                closeMenu();
            }
        });

    })();

    /**
     * Add slimmer style to fixed navigation when scrolled
     */
    (function mastheadScroll(){
        var elem = $('#masthead');
        var stuckClass = 'masthead--fixed';
        $(document).on('scroll', function(e){
            if (window.scrollY > elem[0].clientHeight ) {
                elem.addClass(stuckClass);
            } else {
                elem.removeClass(stuckClass);
            }
        });
    })();

    /**
     * Waypoints. Reveal hidden items and stagger their
     * revelation if there are multiple items to reveal
     */
    (function kaluzaWaypoints(){

        var hiddenSelector = '.kaluza-hidden';
        var revealedClass = 'kaluza-revealed';

        /**
         * Animate copy children, rather than whole parent element,
         * in the event where PHP has no access to copy's children.
         * For this to work, an element whose children need to be animated
         * rather than itself, should have a class containing the word copy
         */
        var copySelector = '[class*=copy]';
        var copyParents = $(hiddenSelector).filter(copySelector);
        if (copyParents.length !== 0) {
            /**
             * Loop through parents who are copy elements and are to be animated
             */
            $(copyParents).each(function(){
                var copyParent = $(this)[0];
                var copyChildren = $(copyParent).children();
                if (copyChildren.length !== 0) {
                    var parentClasses = copyParent.className.split(/\s+/);
                    var hiddenClassRegex = /kaluza\-hidden/;
                    /**
                     * Loop through their children
                     */
                    copyChildren.each(function(){
                        var copyChild = $(this);
                        /**
                         * Loop through the parents classes and add them to their children
                         */
                        $.each(parentClasses, function(key, value){
                            if (!value.search(hiddenClassRegex)) {
                                copyChild.addClass(value);
                            }
                        });
                    });
                    /**
                     * Remove animation classes from the parent
                     */
                    $.each(parentClasses, function(key, value){
                        if (!value.search(hiddenClassRegex)) {
                            $(copyParent).removeClass(value);
                        }
                    });
                }
            });
        }

        /**
         * Use waypoints to reveal hidden elements,
         * staggering them if they have hidden siblings
         * not yet revealed
         */
        var waypoints = $(hiddenSelector).waypoint(
            function(direction) {
                var elem = $(this.element);
                var peers = elem.siblings(hiddenSelector);
                if (peers) {
                    var allPeers = elem.parent().find(hiddenSelector).not(revealedClass);
                    var index = allPeers.index(elem);
                    var delay = ( index + 1 ) * 100;
                    setTimeout(function(){
                        elem.addClass(revealedClass);
                    }, delay);
                } else {
                    elem.addClass(revealedClass);
                }
            },
            { offset: '90%' }
          );
    })();
});