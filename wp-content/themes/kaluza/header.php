<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kaluza
 */

$do = new Kaluza\Functions;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="alternate" href="<?= home_url() ?>" hreflang="en">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/cvn5oui.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header animated">
		<div class="container animated">
			<div class="site-branding">
				<a class="kaluza__logo-anchor kaluza__logo-anchor--light kaluza__logo-anchor--light-horizontal animated" href="<?= get_home_url(); ?>"><?= $do->get_logo( [ 'type' => 'horizontal_light', 'return' => 'img' ] ); ?></a>
				<a class="kaluza__logo-anchor kaluza__logo-anchor--dark kaluza__logo-anchor--dark-horizontal animated" href="<?= get_home_url(); ?>"><?= $do->get_logo( [ 'type' => 'horizontal_dark', 'return' => 'img' ] ); ?></a>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<button class="hamburger hamburger--emphatic" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
