<?php
/**
 * The template for the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Kaluza
 */

get_header();
?>

	<div id="primary" class="content-area" style="background-color: <?= get_field( 'home_bar_colour' ) ?>">

		<main id="main" class="site-main">

			<!-- Mission -->
			<?php if ( have_rows( 'mission_group' ) ) : while ( have_rows( 'mission_group' ) ) : the_row(); ?>
				<div id="kaluza-intro" class="kaluza-intro kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="kaluza__overlay"></div>
					<div class="container">
						<h2 class="kaluza-intro__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ) ?></h2>
						<h1 class="kaluza-intro__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
					</div>
				</div>
			<?php endwhile; endif; ?>

			<!-- Challenge -->
			<?php if ( have_rows( 'challenge_group' ) ) : while ( have_rows( 'challenge_group' ) ) : the_row(); ?>
				<div id="kaluza-vista" class="kaluza-vista kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="kaluza-vista__caption">
						<div class="container">
							<div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--aqua"></div>
							<?php if ( have_rows( 'challenge_card_group' ) ) : while ( have_rows( 'challenge_card_group' ) ) : the_row(); ?>
								<div class="kaluza-card rellax" data-rellax-speed="3">
									<h2 class="kaluza-card__title"><?= get_sub_field( 'title' )?></h2>
									<div class="kaluza-card__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
								</div>
							<?php endwhile; endif; ?>
							<h1 class="kaluza-hidden kaluza-hidden--<?= get_sub_field( 'challenge_title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
						</div>
					</div>

				</div>
			<?php endwhile; endif; ?>

			<!-- What we do -->
			<?php if ( have_rows( 'what_we_do_group' ) ) : while ( have_rows( 'what_we_do_group' ) ) : the_row(); ?>
				<div class="kaluza-screen kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="container">
						<div class="kaluza__overlay">
							<h1 class="kaluza-screen__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
							<h2 class="kaluza-screen__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ); ?></h2>
							<div class="kaluza__bar  kaluza-hidden--extend kaluza__bar--lemon kaluza-hidden"></div>
							<div class="kaluza-screen__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>

			<!-- Awards -->
			<?php if ( have_rows( 'awards_group' ) ) : while ( have_rows( 'awards_group' ) ) : the_row(); ?>
				<div class="kaluza-awards light kaluza__section kaluza__section--fullscreen">
					<div class="container">
						<div class="kaluza-awards__text">
							<h2 class="kaluza-awards__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h2>
							<div class="kaluza-awards__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
						</div>
						<?php if ( have_rows( 'awards_repeater' ) ) : ?>
							<div class="kaluza-awards__awards">
								<?php while ( have_rows( 'awards_repeater' ) ) : the_row(); ?>
									<div class="home-award__award kaluza-hidden kaluza-hidden--<?= get_sub_field( 'animation' ) ?>">
										<img src="<?= get_sub_field( 'logo' )['url'] ?>" alt="<?= get_sub_field( 'title' ) ?>">
										<h5 class="home-award__title">'<?= get_sub_field( 'title' ) ?>'</h5>
										<p class="home-award__subtitle"><?= get_sub_field( 'subtitle' ) ?></p>
									</div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; endif; ?>

			<!-- Leadership -->
			<?php if ( have_rows( 'leadership_group' ) ) : while ( have_rows( 'leadership_group' ) ) : the_row(); ?>
				<div class="home-leaders kaluza__section kaluza__section--fullscreen">
					<div class="container">
						<?php if ( have_rows( 'leaders_repeater' ) ) : ?>
							<div class="home-leaders__leaders">
								<?php while ( have_rows( 'leaders_repeater' ) ) : the_row(); ?>
									<div class="home-leaders__leader animated kaluza-hidden kaluza-hidden--<?= get_sub_field( 'leader_animation' ) ?>">
										<img src="<?= get_sub_field( 'photo' )['url'] ?>" alt="Photo of <?= get_sub_field( 'full_name' ) ?>">
										<h5 class="home-leaders__full-name"><?= get_sub_field( 'full_name' ) ?></h5>
										<h6 class="home-leaders__job-title"><?= get_sub_field( 'job_title' ) ?></h6>
										<div class="kaluza__modal animated">
											<div class="kaluza__modal-inner animated" style="max-width:<?= ( get_sub_field( 'modal_photo' )['width'] * 2 ) . "px" ?>;">
												<div class="kaluza__modal-close animated">
													<img src="<?= get_field( 'close', 'kaluza' )['url'] ?>" alt="Close">
												</div>
												<img class="home-leaders__modal-image home-leaders__modal-image--card" src="<?= get_sub_field( 'modal_photo' )['url'] ?>" alt="Photo of <?= get_sub_field( 'full_name' ) ?>" width="<?= get_sub_field( 'modal_photo' )['width'] ?>" height="<?= get_sub_field( 'modal_photo' )['height'] ?>">
												<img class="home-leaders__modal-image home-leaders__modal-image--button" src="<?= get_sub_field( 'photo' )['url'] ?>" alt="Photo of <?= get_sub_field( 'full_name' ) ?>" width="<?= get_sub_field( 'modal_photo' )['width'] ?>" height="<?= get_sub_field( 'modal_photo' )['height'] ?>">
												<div class="home-leaders__modal-text">
													<h5 class="home-leaders__full-name"><?= get_sub_field( 'full_name' ) ?></h5>
													<h6 class="home-leaders__job-title"><?= get_sub_field( 'job_title' ) ?></h6>
													<div class="home-leaders__description"><?= get_sub_field( 'description' ) ?></div>
												</div>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
						<div class="home-leaders__text">
							<h1 class="home-leaders__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
							<div class="home-leaders__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>

			<!-- Intro to Kaluza -->
			<?php if ( have_rows( 'intro_to_kaluza_group' ) ) : while ( have_rows( 'intro_to_kaluza_group' ) ) : the_row(); ?>
				<div class="kaluza-sheet kaluza__section kaluza__section--fullscreen">
					<div class="container">
						<div class="kaluza-sheet__text">
							<h1 class="kaluza-sheet__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
							<div class="kaluza-sheet__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
							<div class="kaluza__bar kaluza-hidden--extend kaluza__bar--aqua"></div>
						</div>
						<div class="kaluza-sheet__images" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)">
							<img class="kaluza-sheet__foreground-image kaluza-hidden kaluza-hidden--<?= get_sub_field( 'foreground_image_animation' ) ?>" src="<?= get_sub_field( 'foreground_image' )['url'] ?>" alt="Kaluza">
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>

			<!-- Contact Us -->
			<?php if ( have_rows( 'contact_us_group', 'kaluza' ) ) : while ( have_rows( 'contact_us_group', 'kaluza' ) ) : the_row(); ?>
				<div id="contact-us" class="home-contact-us kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="container">
						<h1 class="home-contact-us__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
						<div class="home-contact-us__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ); ?></div>
					</div>
				</div>
			<?php endwhile; endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();