<?php
/**
 * Template Name: What we do
 *
 */
get_header();
?>

	<div id="primary" class="content-area" style="background-color: <?= get_field( 'what_we_do_bar_colour' ) ?>">

		<main id="main" class="site-main">
            <?php while ( have_posts() ) : the_post(); ?>

                <!-- Intro -->
                <?php if ( have_rows( 'what_we_do_intro_group' ) ) : while ( have_rows( 'what_we_do_intro_group' ) ) : the_row(); ?>
                    <div class="kaluza-intro kaluza__section kaluza__section--fullscreen">
                        <div class="parallax__container">
                            <div class="parallax" style="background-image:url(<?= get_sub_field( 'what_we_do_background_image' )['url'] ?>)"></div>
                        </div>
                        <div class="kaluza__overlay"></div>
                        <div class="container">
                            <h1 class="kaluza-intro__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
                        </div>
                    </div>
                <?php endwhile; endif; ?>

                <!-- Platform -->
                <?php if ( have_rows( 'what_we_do_screen_group' ) ) : while ( have_rows( 'what_we_do_screen_group' ) ) : the_row(); ?>
                    <div class="kaluza-platform kaluza-screen kaluza__section kaluza__section--fullscreen">
                        <div class="parallax__container">
                            <div class="parallax" style="background-image:url(<?= get_sub_field( 'what_we_do_screen_background_image' )['url'] ?>)"></div>
                        </div>
                        <div class="container">
                            <div class="kaluza__overlay">
                                <h1 class="kaluza-screen__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_screen_title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
                                <h2 class="kaluza-screen__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_screen_subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ); ?></h2>
                                <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--lemon"></div>
                                <div class="kaluza-screen__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_screen_copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            
                <!-- Partners showcase -->
                <?php if ( have_rows( 'what_we_do_showcase_group' ) ) : while ( have_rows( 'what_we_do_showcase_group' ) ) : the_row(); ?>
                    <div class="kaluza-partners kaluza-showcase kaluza__section kaluza__section--fullscreen">
                            <div class="container">
                                <div class="kaluza-showcase__text">
                                        <h1 class="kaluza-showcase__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_showcase_title_animation' ) ?>"><?= get_sub_field( 'what_we_do_showcase_title' )?></h1>
                                        <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--lemon"></div>
                                        <h2 class="kaluza-showcase__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_showcase_subtitle_animation' ) ?>"><?= get_sub_field( 'what_we_do_showcase_subtitle' ) ?></h2>
                                    </div>
                                    <?php $numberOfColumns = 2; $itemsInEachColumn =  ceil( count( get_sub_field( 'what_we_do_items_repeater' ) ) / $numberOfColumns ); ?>
                                <?php if ( have_rows( 'what_we_do_items_repeater' ) ) : ?>
                                    <div class="kaluza-showcase__items">
                                        <?php while ( have_rows( 'what_we_do_items_repeater' ) ) :  the_row();  ?>
                                            <div class="kaluza-showcase__item kaluza-hidden kaluza-hidden--<?= get_sub_field( 'what_we_do_items_repeater_logo_animation' ) ?>" style="height: calc( 100% / <?= $itemsInEachColumn ?> );">
                                                <img src="<?= get_sub_field( 'what_we_do_items_repeater_logo' )['url'] ?>">
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                <?php endwhile; endif; ?>
                
                <!-- Installation Services -->
                <?php if ( have_rows( 'what_we_do_sheet_group' ) ) : while ( have_rows( 'what_we_do_sheet_group' ) ) : the_row(); ?>
                    <div class="kaluza-points kaluza-sheet kaluza__section kaluza__section--fullscreen">
                        <div class="container">
                            <div class="kaluza-sheet__text">
                                <h1 class="kaluza-sheet__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
                                <div class="kaluza-sheet__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
                                <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--lemon"></div>
                            </div>
                            <?php if ( have_rows( 'what_we_do_points_repeater' ) ) : ?>
                                <div class="kaluza-sheet__points">
                                    <?php while ( have_rows( 'what_we_do_points_repeater' ) ) : the_row(); ?>
                                        <div class="kaluza-sheet__point">
                                            <img class="kaluza-hidden kaluza-hidden--<?= get_sub_field( 'image_animation' ) ?>" src="<?= get_sub_field( 'what_we_do_points_repeater_image' )['url'] ?>">
                                            <div class="kaluza-sheet__point-title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'what_we_do_points_repeater_title' ) ?></div>
                                        </div>
                                    <?php endwhile; ?>

                                </div>
                            <?php endif; ?>


                        </div>
                    </div>
                <?php endwhile; endif; ?>

            <?php endwhile; // end of the loop. ?>

            <!-- Contact Us -->
            
			<?php if ( have_rows( 'contact_us_group', 'kaluza' ) ) : while ( have_rows( 'contact_us_group', 'kaluza' ) ) : the_row(); ?>
				<div id="contact-us" class="home-contact-us kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="container">
						<h1 class="home-contact-us__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
						<div class="home-contact-us__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ); ?></div>
					</div>
				</div>
			<?php endwhile; endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();