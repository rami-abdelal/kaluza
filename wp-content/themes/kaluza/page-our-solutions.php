<?php
/**
 * Template Name: Our Solutions
 *
 */

get_header();
?>

	<div id="primary" class="content-area" style="background-color: <?= get_field( 'our_solutions_bar_colour' ) ?>">

		<main id="main" class="site-main">

            <!-- Our Solutions -->
            <?php while ( have_posts() ) : the_post(); ?>

                <!-- Intro -->
                <?php if ( have_rows( 'intro_group' ) ) : while ( have_rows( 'intro_group' ) ) : the_row(); ?>
                    <div class="kaluza-intro kaluza__section kaluza__section--fullscreen">
                        <div class="parallax__container">
                            <div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
                        </div>
                        <div class="kaluza__overlay"></div>
                        <div class="container">
                            <h1 class="kaluza-intro__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
                        </div>
                    </div>
                <?php endwhile; endif; ?>

                <!-- Software Solutions -->
                <?php if ( have_rows( 'software_solutions_group' ) ) : while ( have_rows( 'software_solutions_group' ) ) : the_row(); ?>
                    <div class="kaluza-solutions kaluza-solutions--software kaluza-sheet kaluza__section">
                        <?php if ( have_rows( 'solutions' ) ) : while ( have_rows( 'solutions' ) ) : the_row(); ?>
                            <div class="container">
                                <div class="kaluza-solution kaluza-solution--<?= get_sub_field( 'alignment' ) ?>">
                                    <div class="kaluza-solution__text">
                                        <h2 class="kaluza-solution__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h2>
                                        <?php if ( ! empty( get_sub_field( 'subtitle' ) ) ) : ?><h3 class="kaluza-solution__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ) ?></h3><?php endif; ?>
                                        <div class="kaluza-solution__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'alignment' ) == 'left' ? 'right' : 'left' ?>"><?= get_sub_field( 'copy' ) ?></div>
                                    </div>
                                    <div class="kaluza-solution__images kaluza-solution__images--<?= get_sub_field( 'background_image_position' ) ?>  kaluza-hidden kaluza-hidden--<?= get_sub_field( 'background_image_animation' ) ?> kaluza-hidden--background" style="background-image: url(<?= get_sub_field( 'background_image' )['url'] ?>);">
                                        <img class="kaluza-hidden kaluza-hidden--delayed kaluza-hidden--<?= get_sub_field( 'foreground_image_animation' ) ?>" src="<?= get_sub_field( 'foreground_image' )['url'] ?>" width="<?= get_sub_field( 'foreground_image' )['width'] ?>" height="<?= get_sub_field( 'foreground_image' )['height'] ?>" alt="<?= get_sub_field( 'title' ) ?>">
                                    </div>
                                    <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--tangerine"></div>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>
                <?php endwhile; endif; ?>

                <!-- Hardware Solutions -->
                <?php if ( have_rows( 'hardware_solutions_group' ) ) : ?>
                    <div class="kaluza-solutions kaluza-solutions--hardware kaluza-sheet kaluza__section">
                        <?php while ( have_rows( 'hardware_solutions_group' ) ) : the_row(); ?>
                            <div class="kaluza__section kaluza__section--fullscreen" style="background-image: url(<?= get_sub_field( 'background_image' )['url'] ?>);">
                                <div class="container">
                                    <h2 class="kaluza-solutions__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h2>
                                    <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--tangerine"></div>
                                    <h3 class="kaluza-solutions__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ) ?></h3>
                                </div>
                            </div>
                            <?php if ( have_rows( 'solutions' ) ) : while ( have_rows( 'solutions' ) ) : the_row(); ?>
                            <div class="container">
                            <div class="kaluza-solution kaluza-solution--<?= get_sub_field( 'alignment' ) ?>">
                                    <div class="kaluza-solution__text">
                                        <h2 class="kaluza-solution__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h2>
                                        <?php if ( get_sub_field( 'subtitle' ) ) ?><h3 class="kaluza-solution__subtitle kaluza-hidden kaluza-hidden--<?= get_sub_field( 'subtitle_animation' ) ?>"><?= get_sub_field( 'subtitle' ) ?></h3>
                                        <div class="kaluza-solution__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
                                    </div>
                                    <?php $top = stripos( strtolower( get_sub_field( 'background_image_position' ) ), 'top' ); ?>
                                    <div class="kaluza-solution__images kaluza-solution__images--<?= get_sub_field( 'background_image_position' ) ?>  kaluza-hidden kaluza-hidden--<?= get_sub_field( 'background_image_animation' ) ?> kaluza-hidden--background" style="background-image: url(<?= get_sub_field( 'background_image' )['url'] ?>);">
                                        <img class="kaluza-hidden kaluza-hidden--delayed kaluza-hidden--<?= get_sub_field( 'foreground_image_animation' ) ?> ?>" src="<?= get_sub_field( 'foreground_image' )['url'] ?>" width="<?= get_sub_field( 'foreground_image' )['width'] ?>" height="<?= get_sub_field( 'foreground_image' )['height'] ?>" alt="<?= get_sub_field( 'title' ) ?>">
                                    </div>
                                    <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--tangerine"></div>
                                </div>
                            </div>
                            <?php endwhile; endif;?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

            <?php endwhile; // end of the loop. ?>

            <!-- Contact Us -->
			<?php if ( have_rows( 'contact_us_group', 'kaluza' ) ) : while ( have_rows( 'contact_us_group', 'kaluza' ) ) : the_row(); ?>
				<div id="contact-us" class="home-contact-us kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="container">
						<h1 class="home-contact-us__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
						<div class="home-contact-us__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ); ?></div>
					</div>
				</div>
			<?php endwhile; endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();