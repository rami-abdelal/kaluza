<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kaluza
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<!-- #secondary 
<aside id="secondary" class="widget-area">
	<?php 
	/**
	 * Sidebars won't be used in this theme. To enable, simply uncomment.
	 *
	 * dynamic_sidebar( 'sidebar-1' );
	 */
	?>
</aside> -->
