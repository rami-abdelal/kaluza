<?php
/**
 * Template Name: Who we work with
 *
 */

get_header();
?>

	<div id="primary" class="content-area" style="background-color: <?= get_field( 'bar_colour' ) ?>">

		<main id="main" class="site-main">
            <?php while ( have_posts() ) : the_post(); ?>
                <!-- intro -->
                <?php if ( have_rows( 'who_we_work_with_intro_group' ) ) : while ( have_rows( 'who_we_work_with_intro_group' ) ) : the_row(); ?>
                    <div class="kaluza-intro kaluza__section kaluza__section--fullscreen">
                        <div class="parallax__container">
                            <div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
                        </div>
                        <div class="kaluza__overlay"></div>
                        <div class="container">
                            <h1 class="kaluza-intro__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
                        </div>
                    </div>
                <?php endwhile; endif; ?>

                <!-- Showcase -->
                <?php if ( have_rows( 'who_we_work_with_showcase_group' ) ) : while ( have_rows( 'who_we_work_with_showcase_group' ) ) : the_row(); ?>
                    <div class="kaluza-showcase kaluza__section kaluza__section--fullscreen">
                            <div class="container">
                                <div class="kaluza-showcase__text">
                                        <h1 class="kaluza-showcase__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' )?></h1>
                                        <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--emerald"></div>
                                        <div class="kaluza-showcase__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ) ?></div>
                                    </div>
                                <?php if ( have_rows( 'items_repeater' ) ) : ?>
                                    <div class="kaluza-showcase__items">
                                        <?php while ( have_rows( 'items_repeater' ) ) : the_row(); ?>
                                            <div class="kaluza-showcase__item">
                                                <div class="kaluza-showcase__images kaluza-hidden">
                                                    <img class="kaluza-showcase__background-image kaluza-hidden kaluza-hidden--<?= get_sub_field( 'background_image_animation' ) ?>" src="<?= get_sub_field( 'background_image' )['url'] ?>" alt="<?= get_sub_field( 'title' ) ?>">
                                                    <img class="kaluza-showcase__foreground-image kaluza-hidden kaluza-hidden--delayed kaluza-hidden--<?= get_sub_field( 'foreground_image_animation' ) ?>" src="<?= get_sub_field( 'foreground_image' )['url'] ?>" alt="<?= get_sub_field( 'title' ) ?>">
                                                </div>
                                                <div class="kaluza-showcase__item-text kaluza-hidden kaluza-hidden--left">
                                                    <h3 class="kaluza-showcase__item-title"><?= get_sub_field( 'title' ) ?></h3>
                                                    <div class="kaluza-showcase__item-copy"><?= get_sub_field( 'copy' ) ?></div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                <?php endwhile; endif; ?>
                </div>

                <!-- Case Study-->
                <?php if ( have_rows( 'who_we_work_with_sheet_group' ) ) : while ( have_rows( 'who_we_work_with_sheet_group' ) ) : the_row(); ?>
                    <div class="kaluza-sheet kaluza__section kaluza__section--fullscreen">
                        <div class="container">
                            <div class="kaluza-card">
                                <h1 class="kaluza-card__title"><?= get_sub_field( 'who_we_work_with_sheet_group_title' ) ?></h1>
                                <div class="kaluza-card__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'who_we_work_with_sheet_group_copy' ) ?></div>
                                <div class="kaluza__bar kaluza-hidden kaluza-hidden--extend kaluza__bar--aqua"></div>
                            </div>
                            <div class="kaluza-card__video">
                                <iframe src="https://www.youtube.com/embed/sveqqIU0eBw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>

                <?php endwhile; // end of the loop. ?>

            <!-- Contact Us -->
            
			<?php if ( have_rows( 'contact_us_group', 'kaluza' ) ) : while ( have_rows( 'contact_us_group', 'kaluza' ) ) : the_row(); ?>
				<div id="contact-us" class="home-contact-us kaluza__section kaluza__section--fullscreen">
					<div class="parallax__container">
						<div class="parallax" style="background-image:url(<?= get_sub_field( 'background_image' )['url'] ?>)"></div>
					</div>
					<div class="container">
						<h1 class="home-contact-us__title kaluza-hidden kaluza-hidden--<?= get_sub_field( 'title_animation' ) ?>"><?= get_sub_field( 'title' ) ?></h1>
						<div class="home-contact-us__copy kaluza-hidden kaluza-hidden--<?= get_sub_field( 'copy_animation' ) ?>"><?= get_sub_field( 'copy' ); ?></div>
					</div>
				</div>
			<?php endwhile; endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();