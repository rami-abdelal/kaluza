<?php
/**
 * Include Advanced Custom Fields plugin for use within the theme
 *
 * @package Kaluza
 */

namespace Kaluza;
class ACF {

    public $folder = '/vendor/advanced-custom-fields-pro/';

    public function __construct()
    {

        /**
         * Change ACF path and directory using filters, allowing us to
         * use ACF within our theme.
         */
        add_filter( 'acf/settings/path', [$this, 'change_path'] );
        add_filter( 'acf/settings/dir', [$this, 'change_dir'] );

        /**
         * Uncomment the filter below to hide the ACF menu item,
         * though this will usually be showing, so keep it commented
         * if you want to change any fields.
         * 
         * add_filter( 'acf/settings/show_admin', '__return_false' );
         */ 

        /**
         * Initialise ACF
         */
        include_once( get_stylesheet_directory() . $this->folder . '/acf.php' );


    }

    /**
     * Change path
     */
    public function change_path( $path )
    {
        $path = get_stylesheet_directory() . $this->folder;
        return $path;
    }

    /**
     * Change directory
     */
    public function change_dir( $dir )
    {
        $dir = get_stylesheet_directory_uri() . $this->folder;
        return $dir;
    }

}

/**
 * Instantiate
 */
new ACF;
?>